<!DOCTYPE html>
<html lang="fr">
<!--Créer un formulaire sur le nom et le prénom . 
Ce formulaire doit être redirigé vers la page utilisateur.php
 avec la méthode GET-->
 <head>
 <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <meta http-equiv="X-UA-Compatible" content="ie=edge"><!--//correspondance avec Internet Explorer -->
 </head>
<body>
    <?php
if(isset($_GET['nom']) && isset($_GET['prenom'])) {
echo 'Bonjour monsieur ou madame'." ".$_GET['nom']." ".$_GET['prenom'];
} else{ 
    echo "Veuillez renseigner le prénom et le nom à la fois";
}
?>
</body>
<!--Pour tester, je vais dans mon dossier 
 où sont mes .php puis je démarre le serveur dans le terminal php avec:     php -S localhost:8000 ; pour 
 le voir en ligne je tape dans le navigateur localhost:8000 pour afficher la page
 
En méthode get, les infos envoyées grace à submit à la page utilisateur.php pour traitement. 
Les informations sont transmises dans l'adresse URL;
par exemple la transmission des données est faite comme ça http://localhost:8000/utilisateur.php?nom=toto&prenom=tirgou
-->
</html>
<?php