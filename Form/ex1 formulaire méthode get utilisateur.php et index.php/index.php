Ce formulaire doit être redirigé vers la page utilisateur.php avec la méthode GET -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Formulaire index.php</title>
</head>
<body>
    <form action="utilisateur.php" method="get">
    <input type="text" name="nom" id="" placeholder="Entre ton nom">
    <input type="text" name="prenom" id="" placeholder="Entre ton prénom">
    <button type="submit">Envoyer mes données à php avec la méthode get</button>
    </form>
</body>
<!--Pour tester, je vais dans mon dossier 
 où sont mes .php puis je démarre le serveur dans le terminal php avec php -S localhost:8000 ; pour 
 le voir en ligne je tape dans le navigateur localhost:8000 pour afficher la page
 
En méthode get, les infos envoyées grace à submit à la page utilisateur.php pour traitement. 
Les informations sont transmises dans l'adresse URL; ex pour une recherche google, les choses recherchées apparissent dans la barre c'est en Get
par exemple la transmission des données est faite comme ça http://localhost:8000/utilisateur.php?nom=toto&prenom=tirgou
-->
</html>

