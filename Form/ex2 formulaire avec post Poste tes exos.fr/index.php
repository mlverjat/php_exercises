<!-- ConsigneCréer un formulaire sur le nom et le prénom .
 Ce formulaire doit être redirigé vers la page utilisateur.php avec la méthode POST -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Formulaire index.php</title>
</head>
<body>
    <form action="utilisateur.php" method="post">
    <input type="text" name="nom" id="" placeholder="Entre ton nom">
    <input type="text" name="prenom" id="" placeholder="Entre ton prénom">
    <button type="submit">Envoyer mes données à php avec la méthode post</button>
    </form>
</body>
<!--Pour tester, je vais dans mon dossier 
 où sont mes .php puis je démarre le serveur dans le terminal php avec php -S localhost:8000 ; pour 
 le voir en ligne je tape dans le navigateur localhost:8000 pour afficher la page
 
En méthode post, les infos envoyées grace à submit à la page utilisateur.php pour traitement. 
Les informations ne sont pas transmises dans l'adresse URL;
par exemple la transmission des données est faite comme ça http://localhost:8000/utilisateur.php
C'est donc la différence, post est farceur , il cache les informations.

post : les données ne transiteront pas par l'URL, l'utilisateur ne les verra donc pas passer dans
 la barre d'adresse. Cette méthode permet d'envoyer autant de données que l'on veut, ce qui fait qu'on 
 la privilégie le plus souvent. Néanmoins, les données ne sont pas plus sécurisées qu'avec la 
 méthodeGETet il faudra toujours vérifier si tous les paramètres sont bien présents et valides, 
 comme on l'a fait dans le chapitre précédent. On ne doit pas plus faire confiance aux formulaires 
 qu'aux URL.
 https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/913099-
 transmettez-des-donnees-avec-les-formulaires
-->
</html>
